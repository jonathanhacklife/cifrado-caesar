﻿import { cifrar } from './cifrado.js';

//#region ROTAR RUEDA
let rotateAngle = 0;
var iteracion = 0;

export function rotate(element, direccion) {
  if (direccion) {
    rotateAngle = rotateAngle - 13.35; //Anterior: 13.85
    iteracion++
    if (rotateAngle <= -360) {
      rotateAngle = 0;
      iteracion = 0;
    }
  } else {
    rotateAngle = rotateAngle + 13.35; //Anterior: 13.85
    iteracion--
    if (rotateAngle >= 360) {
      rotateAngle = 0;
      iteracion = 0;
    }
  }

  element.setAttribute("style", "transform: rotate(" + rotateAngle + "deg)");
  document.getElementById("primerFraseInput").value = cifrar(document.getElementById("primerFrase").innerHTML, iteracion)
  document.getElementById("segundaFraseInput").value = cifrar(document.getElementById("segundaFrase").innerHTML, iteracion)
}
//#endregion

//#region ROTAR TEXTO
export function rotarTexto(elemento, radio) {
  const degreeToRadian = (angle) => {
    return angle * (Math.PI / 180);
  };

  const radius = radio;
  const diameter = radius * 2;

  const circle = elemento;
  circle.style.width = `${diameter}px`;
  circle.style.height = `${diameter}px`;

  const text = circle.innerText;
  const characters = text.split("");
  circle.innerText = null;

  const startAngle = -90;
  const endAngle = 270;
  const angleRange = endAngle - startAngle;

  const deltaAngle = angleRange / characters.length;
  let currentAngle = startAngle;

  characters.forEach((char, index) => {
    const charElement = document.createElement("span");
    charElement.innerText = `${char}\n${index}`;
    const xPos = radius * (1 + Math.cos(degreeToRadian(currentAngle)));
    const yPos = radius * (1 + Math.sin(degreeToRadian(currentAngle)));

    const transform = `translate(${xPos}px, ${yPos}px)`;
    const rotate = `rotate(${index * deltaAngle}deg)`;
    charElement.style.transform = `${transform} ${rotate}`;

    currentAngle += deltaAngle;
    circle.appendChild(charElement);
  });
}