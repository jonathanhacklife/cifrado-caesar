﻿import { abecedario } from './constants.js';
//#region CIFRADO
export var cifrado = false;
export let cifrar = (texto = "Test", clave = 0) => {
  const normalizarTexto = (str) => str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase();

  texto = normalizarTexto(texto);

  if (clave > abecedario.length)
    console.log("La clave ingresada no es una clave válida");
  else
    clave = abecedario.length - clave

  let textoCifrado = "";

  for (let letra of texto)
    if (abecedario.includes(letra)) {
      let pos_letra = abecedario.indexOf(letra);
      let nueva_pos = (pos_letra += clave) % abecedario.length;
      textoCifrado += abecedario[nueva_pos];
    } else {
      textoCifrado += letra
    }
  return textoCifrado
}
//#endregion