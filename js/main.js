import { rotate, rotarTexto } from './charWheel.js';
import { abecedario, frase1, frase2 } from './constants.js';

document.getElementById("descifrar").addEventListener('click', () => { rotate(document.getElementById('circleBase'), false) });
document.getElementById("cifrar").addEventListener('click', () => { rotate(document.getElementById('circleBase'), true) });
document.getElementById("circleBase").addEventListener('click', () => { rotate(document.getElementById('circleBase'), true) });

document.getElementById('primerFrase').innerHTML = prompt('Ingrese la primer frase');
document.getElementById('segundaFrase').innerHTML = prompt('Ingrese la segunda frase');

if (document.getElementById('primerFrase').innerHTML == "")
  document.getElementById('primerFrase').innerHTML = frase1;
if (document.getElementById('segundaFrase').innerHTML == "")
  document.getElementById('segundaFrase').innerHTML = frase2;

document.getElementById('circular-text-base').innerHTML = `<div id="circular-text-base">${abecedario}</div>`
document.getElementById('circular-text').innerHTML = `<div id="circular-text">${abecedario}</div>`

rotarTexto(document.getElementById("circular-text"), 125)
rotarTexto(document.getElementById("circular-text-base"), 150)