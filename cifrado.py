﻿class Cifrado_Caesar:
    """
    Cifrado Caesar: Es uno de los métodos más simples y más usados en el mundo para cifrar textos.
    """

    def __init__(self):
        # Abecedario a utilizar en el cifrado
        self.abecedario = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"

    def cifrar(self, texto="", clave=0):
        # Obtenemos mensaje a cifrar desde el usuario
        # llamamos a upper para obtener sólo mayúsculas
        if texto != "":
            self.texto = texto.upper()
        else:
            self.texto = input("Mensaje: ").upper()

        self.clave = int(
            input("Desplazamiento (0 - {}): ".format(len(self.abecedario) - 1)))

        # Pedimos al usuario la cantidad de desplazamiento
        if self.clave > len(self.abecedario):
            print("No es una clave válida")

        # Variable para guardar mensaje cifrado
        self.cifrado = ""
        # Iteramos por cara letra del mensaje
        for letra in self.texto:
            # Si la letra está en el abecedario se reemplaza
            if letra in self.abecedario:
                self.pos_letra = self.abecedario.index(letra)
                # Sumamos para movernos a la derecha del abecedario
                self.nueva_pos = (
                    self.pos_letra + self.clave) % len(self.abecedario)
                self.cifrado += self.abecedario[self.nueva_pos]
            else:
                # Si no está en el abecedario sólo lo agrega
                self.cifrado += letra
        print("Mensaje original: {}\nMensaje cifrado: {}".format(
            self.texto, self.cifrado))

    def descifrar(self, texto=""):
        # Obtenemos mensaje a descifrar desde el usuario
        # llamamos a upper para obtener sólo mayúsculas
        if texto != "":
            self.texto = texto.upper()
            # Iteramos por posibles valores de desplazamiento
            for posible_valor in range(len(self.abecedario)):
                # Guardar posible mensaje
                self.descifrado = ""
                for letra in self.texto:
                    # Si la letra está en el abecedario reemplazamos
                    if letra in self.abecedario:
                        self.pos_letra = self.abecedario.index(letra)
                        # Restamos para movernos a la izquierda
                        self.nueva_pos = (
                            self.pos_letra - posible_valor) % len(self.abecedario)
                        self.descifrado += self.abecedario[self.nueva_pos]
                    else:
                        self.descifrado += letra
                self.mensaje = "ROT -> {}: {}".format(
                    posible_valor, self.descifrado)
                print(self.mensaje)
        else:
            print("[ERROR]: Debe ingresar al menos un código o caracter")


Cifrado_Caesar().cifrar()
#Cifrado_Caesar().descifrar("IHÑLC FXOSÑHDQRV")
